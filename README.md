# Semesterprojekt

In diesem Dokument die Hausarbeit für das Modul "Einführung in Web Engineering" (Prof. Dr. Manfred Kaul) im Wintersemester 2020/2021 vorgestellt.

Mit diesem Projekt zeige ich, dass ich die erlernten Konzepte und Verfahren der Webentwicklung anwenden kann.

Thema des Projektes war die Erstellung einer Homepage, welche als "Solution-Navigator" dienen soll. 

## Details
Die Seite wurde vollständig in Typescript und dem Framework ReactJS geschrieben.
Weitere Bibliotheken die benutzt wurden sind MaterialUI als Komponentenbibliothek, react-count-up, react-page-transition, react-router-dom, react-syntax-highlighter und axios.

## Quellen 

Hier sind die Quellen der im Projekt benutzten Bilder.

### Background & Mobile

https://wallpaperaccess.com/black-white-minimalist

### introduction

https://unsplash.com/photos/OqtafYT5kTw

### CSS1

https://unsplash.com/photos/6JVlSdgMacE

### css2

https://unsplash.com/photos/_yMciiStJyY

### javascript

https://unsplash.com/s/photos/javascript

### ecma

https://levelup.gitconnected.com/es6-spread-explained-e4d75450ea50

### functional

https://www.leadingagile.com/wp-content/uploads/2018/02/When-functional-programming-isnt.jpg

### DOM

https://miro.medium.com/max/1120/0*dcpjTwj_qSjTdaUc.jpg

### async

https://orlyapps.de/blog/web/asyncawait-asynchrone-javascript-programmierung

### vue.js

https://wallpaperaccess.com/vue

### php

https://wallpaperaccess.com/php

### Bert Bos

https://alchetron.com/Bert-Bos

### hakon lie

https://en.wikipedia.org/wiki/H%C3%A5kon_Wium_Lie

### Gitlab icon

https://about.gitlab.com/press/press-kit/

### H-BRS Logo

https://www.h-brs.de/de/seminararbeiten-praxisberichte-abschlussarbeiten
