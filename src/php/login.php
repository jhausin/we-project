<?php
require_once("./cors.php");

cors();
$login = new LoginService();
$user = json_decode(file_get_contents('php://input'), true);
echo json_encode(login()); 


function login(){
    global $login;
    global $user;
    if(!$login->verifyUser($user["username"], $user["password"])){
        return [
            "login" => false
        ];
    }
    return [
    "login" => true
    ];
}
    
    class LoginService {
    private $userdata;
    function __construct() {
        $this->userdata = json_decode(file_get_contents("./userlist.json"), true);
    }
    private function getUser($username) {
        foreach ($this->userdata as $registered_username => $password) {
            if ($registered_username === $username) {
                return [
                    "username" => $registered_username,
                    "password" => $password,
                ];
            }
        }
        return null;
    }
    public function verifyUser($username, $password){
        $user = $this->getUser($username);
        return $user ?  password_verify($password, $user["password"]) : false;
    }
}
