<<?php

require_once("./cors.php");

class RegistrationService {
    private $userdata;
    function __construct() {
        $this->userdata = json_decode(file_get_contents("./userlist.json"), true);
    }
    public function alreadyRegistered($username) {
        return $this->getUser($username) !== null;
    }

    private function getUser($username) {
        foreach ($this->userdata as $registered_username => $password) {
            if ($registered_username === $username) {
                return [
                    "username" => $registered_username,
                    "password" => $password,
                ];
            }
        }
        return null;
    }

    public function saveUserToJSON($username, $password) {
        $this->userdata[$username] = password_hash($password, PASSWORD_BCRYPT);
        file_put_contents("./userlist.json", json_encode($this->userdata));
    }
}
cors();
$registration = new RegistrationService();
$user = json_decode(file_get_contents('php://input'), true);

if (!$user["username"] || !$user["password"]) {
    http_response_code(409);
    return;
}
return register();

function register() {
    global $registration;
    global $user;
    if ($registration->alreadyRegistered($user["username"])) {
        http_response_code(409);
        return [
            "registration" => false
        ];
    }
    $registration->saveUserToJSON($user["username"], $user["password"]);
    return [
        "registration" => true
    ];
}





