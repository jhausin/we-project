<?php

require_once("./cors.php");
cors();
$data = json_decode(file_get_contents("./navigator_content.json"), true);
switch($_SERVER["REQUEST_METHOD"]){
    case "GET": 
        echo json_encode($data);
        break;
    case "POST":
         $new_data = json_decode(file_get_contents("php://input"),true);
         $data[$new_data["topic"]][$new_data["category"]]["content"] = $new_data["content"];
         file_put_contents("./navigator_content.json", json_encode($data));
         echo [ "saved" => true,];
         break;
    }