import React from "react";
import { Grid } from "@material-ui/core";
import TaskCard from "./TaskCard/TaskCard";

import styles from "./Projects.module.scss";

const Projects: React.FC = () => {
    return (
        <div className={styles.container}>
            <Grid container spacing={3} justify='center'>
                <TaskCard exerciseNr={1} title='Einführung' logo='introduction' />
                <TaskCard exerciseNr={2} title='CSS Teil 1' logo='css1' />
                <TaskCard exerciseNr={3} title='CSS Teil 2' logo='css2' />
                <TaskCard exerciseNr={4} title='JavaScript' logo='javascript' />
                <TaskCard exerciseNr={5} title='EcmaScript' logo='ecma' />
                <TaskCard exerciseNr={6} title='Functional' logo='functional' />
                <TaskCard exerciseNr={7} title='DOM' logo='dom' />
                <TaskCard exerciseNr={8} title='Async' logo='async' />
                <TaskCard exerciseNr={9} title='Vue.js' logo='vue' />
                <TaskCard exerciseNr={10} title='PHP' logo='php' />
            </Grid>
        </div>
    );
};

export default Projects;
