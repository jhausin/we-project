import { Typography, Card, CardContent, Grid, CardMedia } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import styles from "./TaskCard.module.scss";
interface TaskCardProps {
    exerciseNr: number;
    title: string;
    logo: string;
}

const TaskCard: React.FC<TaskCardProps> = (props: TaskCardProps) => {
    const { exerciseNr, title, logo } = props;
    const path = `projects/exercise${exerciseNr}`;
    const imgPath = `${process.env.PUBLIC_URL}/${logo}.jpeg`;

    return (
        <Grid item xs={12} sm={4} md={3} component={Card} className={styles.card}>
            <CardContent className={styles.content}>
                <CardMedia className={styles.img} image={imgPath} title={title} />
                <Typography className={styles.title} variant='h5' component='h2'>
                    {title}
                    <Typography variant='caption' color='textSecondary'>
                        {" "}
                        - Übung {exerciseNr}
                    </Typography>
                </Typography>
                <Link className={styles.link} to={path}>
                    Zur Übung
                </Link>
            </CardContent>
        </Grid>
    );
};

export default TaskCard;
