export const vorrang: string = `class Vorrang {
    arr;
    solution;
    constructor(arr) {
        this.arr = arr;
        this.calculate();
    }
    private calculate() {
        this.solution = this.topsort(this.arr);
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    private topsort(arr) {
        const sort = [];
        while (arr.length > 0) {
            arr.forEach(deps => {
                if (this.hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            });
        }
        return sort;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);
console.assert(vorrang.solution.indexOf("schlafen") < vorrang.solution.indexOf("essen"));
console.assert(vorrang.solution.indexOf("essen") < vorrang.solution.indexOf("studieren"));
console.assert(vorrang.solution.indexOf("studieren") < vorrang.solution.indexOf("prüfen"));`;
export const iterierbarkeit: string = `class Vorrang {
    arr;
    solution;
    constructor(arr) {
        this.arr = arr;
    }
    next() {
        for (const deps of this.arr) {
            if (this.hasNoDependency(deps[0], this.arr)) {
                // remove it from arr.
                const value = deps[0];
                this.arr = this.arr.map(d => d.filter(task => task !== value));
                this.arr = this.arr.filter(d => d.length > 0);
                return { value, done: false };
            }
        }
        //
        return { value: undefined, done: true };
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    [Symbol.iterator]() {
        return this;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);
const solution = ["schlafen", "essen", "studieren", "prüfen"];
let index = 0;
for (const el of vorrang) {
    console.assert(solution[index] === el);
    index++;
}`;
export const generator: string = `class Vorrang {
    arr;
    solution;
    cursor;
    constructor(arr) {
        this.arr = arr;
        this.cursor = 0;
        this.calculate();
    }
    private calculate() {
        this.solution = this.topsort(this.arr);
    }
    public *sol() {
        yield this.solution[this.cursor];
        this.cursor++;
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    private topsort(arr) {
        const sort = [];
        while (arr.length > 0) {
            arr.forEach(deps => {
                if (this.hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            });
        }
        return sort;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);
const solution = ["schlafen", "essen", "studieren", "prüfen"];
let index = 0;
for (const el of vorrang.sol()) {
    console.assert(solution[index] === el);
    index++;
}`;
export const proxy: string = `class Vorrang {
    arr;
    solution;
    cursor;
    constructor(arr) {
        this.arr = arr;
        this.cursor = 0;
        this.calculate();
    }
    private calculate() {
        this.solution = this.topsort(this.arr);
    }
    public nextElement() {
        const el = this.solution[this.cursor];
        this.cursor++;
        return el;
    }
    private hasNoDependency(el, arr) {
        return !arr.some(deps => deps.length > 1 && deps[1] === el);
    }
    private topsort(arr) {
        const sort = [];
        while (arr.length > 0) {
            arr.forEach(deps => {
                if (this.hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    // remove it from arr.
                    arr = arr.map(d => d.filter(task => task !== deps[0]));
                    arr = arr.filter(d => d.length > 0);
                }
            });
        }
        return sort;
    }
}
const vorrang = new Vorrang([
    ["schlafen", "studieren"],
    ["essen", "studieren"],
    ["studieren", "prüfen"],
]);
const proxy = new Proxy(vorrang, {
    get(target, p): any {
        if (p === "nextElement") {
            console.log("Elements left: ", target.solution.length - (target.cursor + 1));
        }
        return target[p];
    },
});
console.log(proxy.nextElement());`;
export const deepcopy: string = `const deepCopy = struct => {
    return struct instanceof Array
        ? struct.map(deepCopy)
        : typeof struct == "object"
        ? Object.fromEntries(struct.entries(deepCopy))
        : struct;
};
const arr = [[1, 2, 3]];
const other = deepCopy(arr);
other[0][0] = 2;
console.assert(other[0][0] === 2);
console.assert(arr[0][0] === 1);`;
