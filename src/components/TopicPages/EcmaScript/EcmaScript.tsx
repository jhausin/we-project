import React, { useState } from "react";
import { Typography, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";

import { vorrang, iterierbarkeit, generator, proxy, deepcopy } from "./sourcecodes";
import styles from "./EcmaScript.module.scss";

const EcmaScript: React.FC = () => {
    const [index, setIndex] = useState(0);
    return (
        <div className={styles.container}>
            <Typography align='center' variant='h4' gutterBottom>
                EcmaScript
            </Typography>
            <Typography variant='body1' align='center' gutterBottom>
                In dieser Übung wurden keine Web-Implementierungen erwartet. Die Topologische Sortierung kann{" "}
                <Link to='/projects/exercise7' style={{ textDecoration: "none" }}>
                    hier{" "}
                </Link>
                unter "Topsort Webapp" getestet werden.
            </Typography>
            <div className={styles.contentContainer}>
                <Typography align='center' color='textSecondary' variant='h5' gutterBottom>
                    Quellcodes der Übung:
                </Typography>
                <div className={styles.buttons}>
                    <Button className={styles.button} variant='contained' color='primary' onClick={() => setIndex(1)}>
                        Klasse für Vorrangrelationen
                    </Button>
                    <Button className={styles.button} variant='contained' color='primary' onClick={() => setIndex(2)}>
                        Toplogische Iterierbarkeit
                    </Button>
                    <Button className={styles.button} variant='contained' color='primary' onClick={() => setIndex(3)}>
                        Topologischer Generator
                    </Button>
                    <Button className={styles.button} variant='contained' color='primary' onClick={() => setIndex(4)}>
                        Proxy
                    </Button>
                    <Button className={styles.button} variant='contained' color='primary' onClick={() => setIndex(5)}>
                        DeepCopy
                    </Button>
                </div>
                <div className={styles.sourcecodes}>
                    <div hidden={index !== 1} style={{ maxWidth: "90%" }}>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {vorrang}
                        </SyntaxHighlighter>
                    </div>
                    <div hidden={index !== 2} style={{ maxWidth: "90%" }}>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {iterierbarkeit}
                        </SyntaxHighlighter>
                    </div>
                    <div hidden={index !== 3} style={{ maxWidth: "90%" }}>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {generator}
                        </SyntaxHighlighter>
                    </div>
                    <div hidden={index !== 4} style={{ maxWidth: "90%" }}>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {proxy}
                        </SyntaxHighlighter>
                    </div>
                    <div hidden={index !== 5} style={{ maxWidth: "90%" }}>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {deepcopy}
                        </SyntaxHighlighter>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EcmaScript;
