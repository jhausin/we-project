import React from "react";
import styles from "./Task2.module.scss";

const Task2: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={value !== index} className={styles.container}>
            <input type='checkbox' />
            Hide and show via Checkbox - CSS ONLY
            <div className={styles.imgContainer}>
                <img
                    alt='Bild der Hochschule'
                    src='https://upload.wikimedia.org/wikipedia/commons/a/a4/Hochschule_Bonn-Rhein-Sieg_Wolfgang_G%C3%B6ddertz_Induktion.jpg'
                />
            </div>
        </div>
    );
};

export default Task2;
