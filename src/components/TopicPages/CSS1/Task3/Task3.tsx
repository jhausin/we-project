import React, { useState } from "react";
import {
    TableContainer,
    Table,
    TableRow,
    TableCell,
    TableBody,
    Typography,
    TextField,
    MenuItem,
    Select,
    FormControl,
    InputLabel,
    FormControlLabel,
    Radio,
    Checkbox,
    RadioGroup,
} from "@material-ui/core";
import styles from "./Task3.module.scss";

const Task3: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    const [role, setRole] = useState("");
    const [recommend, setRecommend] = useState("");
    const [like, setLike] = useState("");

    const handleRoleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setRole(event.target.value as string);
    };
    const handleRecommendChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setRecommend(event.target.value as string);
    };
    const handleLikeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setLike(event.target.value as string);
    };
    return (
        <TableContainer hidden={value !== index} className={styles.container}>
            <Typography variant='body1' align='center'>
                Let us know how we can improve freeCodeCamp
            </Typography>
            <Table className={styles.table}>
                <TableBody>
                    <TableRow>
                        <TableCell style={{ width: "50%" }} align='right'>
                            * Name:
                        </TableCell>
                        <TableCell align='left'>
                            <TextField label='Enter your Email' variant='outlined' />
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>* Email:</TableCell>
                        <TableCell>
                            <TextField label='Enter your Name' variant='outlined' />
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>* Age:</TableCell>
                        <TableCell>
                            <TextField type='number' label='Enter your Age' variant='outlined' />
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>Which option best describes your current role?</TableCell>
                        <TableCell>
                            <FormControl variant='outlined' className={styles.formControl}>
                                <InputLabel id='role'>Role</InputLabel>
                                <Select
                                    autoWidth
                                    labelId='role'
                                    label='role'
                                    value={role}
                                    onChange={handleRoleChange}
                                    className={styles.select}
                                >
                                    <MenuItem value='Student'>Student</MenuItem>
                                    <MenuItem value='Employee'>Employee</MenuItem>
                                    <MenuItem value='Employer'>Employer</MenuItem>
                                    <MenuItem value='Unemployed'>Unemployed</MenuItem>
                                </Select>
                            </FormControl>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>
                            * How likely is that you would recommend <br />
                            freeCodeCamp to a friend?
                        </TableCell>
                        <TableCell>
                            <RadioGroup
                                aria-label='recommend'
                                name='recommend'
                                value={recommend}
                                onChange={handleRecommendChange}
                            >
                                <FormControlLabel
                                    value='definitely'
                                    control={<Radio color='primary' />}
                                    label='Definitely'
                                />
                                <FormControlLabel value='maybe' control={<Radio color='primary' />} label='Maybe' />
                                <FormControlLabel
                                    value='not sure'
                                    control={<Radio color='primary' />}
                                    label='Not Sure'
                                />
                            </RadioGroup>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>What do you like the most in FCC</TableCell>
                        <TableCell>
                            <FormControl variant='outlined' className={styles.formControl}>
                                <InputLabel id='like'>Like the most</InputLabel>
                                <Select
                                    autoWidth
                                    labelId='like'
                                    label='Like the most'
                                    value={like}
                                    onChange={handleLikeChange}
                                    className={styles.select}
                                >
                                    <MenuItem value='Tutorials'>Tutorials</MenuItem>
                                    <MenuItem value='Projects'>Projects</MenuItem>
                                    <MenuItem value='Exercises'>Exercises</MenuItem>
                                </Select>
                            </FormControl>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align='right'>
                            Things that should be improved in the future <br />
                            (Check all that apply)
                        </TableCell>
                        <TableCell>
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <FormControlLabel label='Front-End Projects' control={<Checkbox color='primary' />} />
                                <FormControlLabel label='Back-End Projects' control={<Checkbox color='primary' />} />
                                <FormControlLabel label='Data Visualization' control={<Checkbox color='primary' />} />
                            </div>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default Task3;
