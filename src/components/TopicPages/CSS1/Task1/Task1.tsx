import React from "react";
import { Grid } from "@material-ui/core";

import styles from "./Task1.module.scss";

const Task1: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div className={styles.container} hidden={value !== index}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <h1 className={styles.header}>Erste Überschrift</h1>
                    <div className={styles.content}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Erat pellentesque adipiscing commodo elit at imperdiet.
                            Aliquam ut porttitor leo a diam sollicitudin tempor id. Integer feugiat scelerisque varius
                            morbi enim nunc. Molestie nunc non blandit massa enim nec dui. Augue eget arcu dictum varius
                            duis. Amet aliquam id diam maecenas ultricies. Erat pellentesque adipiscing commodo elit at
                            imperdiet dui accumsan. Massa sed elementum tempus egestas sed sed risus pretium quam.
                        </p>
                        <p>
                            Sit amet mauris commodo quis imperdiet massa. In hendrerit gravida rutrum quisque non tellus
                            orci. Eu lobortis elementum nibh tellus molestie nunc non blandit massa. Molestie a iaculis
                            at erat. Nam aliquam sem et tortor consequat. Pharetra sit amet aliquam id diam. Urna nec
                            tincidunt praesent semper feugiat nibh sed. Nec ullamcorper sit amet risus nullam eget felis
                            eget nunc. Volutpat lacus laoreet non curabitur gravida. Arcu vitae elementum curabitur
                            vitae.
                        </p>
                        <p>
                            Porta nibh venenatis cras sed felis eget velit aliquet. Posuere urna nec tincidunt praesent
                            semper feugiat nibh sed. Nisl condimentum id venenatis a condimentum vitae. Turpis egestas
                            integer eget aliquet nibh. Tristique nulla aliquet enim tortor at auctor urna nunc id. Est
                            lorem ipsum dolor sit. Aliquet enim tortor at auctor urna nunc. Auctor urna nunc id cursus
                            metus aliquam eleifend. Non pulvinar neque laoreet suspendisse interdum. Erat velit
                            scelerisque in dictum. Suspendisse sed nisi lacus sed viverra tellus in hac. Turpis massa
                            sed elementum tempus egestas. Bibendum neque egestas congue quisque egestas diam in arcu
                            cursus.
                        </p>
                        <p>
                            Tellus mauris a diam maecenas sed enim ut sem. Ut sem viverra aliquet eget sit. Nec dui nunc
                            mattis enim ut tellus elementum sagittis vitae. Sit amet facilisis magna etiam tempor orci
                            eu lobortis. Accumsan lacus vel facilisis volutpat est velit egestas dui id. Non enim
                            praesent elementum facilisis leo. Vitae aliquet nec ullamcorper sit amet risus nullam eget.
                            Leo vel orci porta non pulvinar neque laoreet suspendisse. In ornare quam viverra orci
                            sagittis eu volutpat odio. Mi ipsum faucibus vitae aliquet nec ullamcorper sit. Dolor sit
                            amet consectetur adipiscing elit. Condimentum vitae sapien pellentesque habitant morbi
                            tristique senectus et. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu.
                        </p>
                        <p>
                            Blandit aliquam etiam erat velit scelerisque in dictum non. Eget sit amet tellus cras
                            adipiscing enim eu. Feugiat vivamus at augue eget arcu dictum varius duis at. Turpis egestas
                            pretium aenean pharetra magna ac placerat. Commodo nulla facilisi nullam vehicula ipsum a.
                            Felis donec et odio pellentesque. Sit amet mattis vulputate enim. Mi tempus imperdiet nulla
                            malesuada pellentesque elit eget gravida. Nisi est sit amet facilisis magna etiam tempor.
                            Rhoncus aenean vel elit scelerisque mauris pellentesque. Justo nec ultrices dui sapien eget
                            mi proin sed. Nunc mattis enim ut tellus elementum sagittis vitae. Vulputate mi sit amet
                            mauris commodo quis imperdiet massa. Eros in cursus turpis massa. Vitae semper quis lectus
                            nulla at volutpat diam ut. Eu volutpat odio facilisis mauris. Nulla at volutpat diam ut
                            venenatis tellus in metus. Pharetra vel turpis nunc eget lorem dolor. Mattis rhoncus urna
                            neque viverra justo. Amet nulla facilisi morbi tempus iaculis urna id volutpat. Nibh ipsum
                            consequat nisl vel. Pellentesque habitant morbi tristique senectus et netus et malesuada
                            fames. Nec feugiat in fermentum posuere urna nec tincidunt praesent.
                        </p>
                    </div>
                    <h1 className={styles.header}>Zweite Überschrift</h1>
                    <div className={styles.content}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Erat pellentesque adipiscing commodo elit at imperdiet.
                            Aliquam ut porttitor leo a diam sollicitudin tempor id. Integer feugiat scelerisque varius
                            morbi enim nunc. Molestie nunc non blandit massa enim nec dui. Augue eget arcu dictum varius
                            duis. Amet aliquam id diam maecenas ultricies. Erat pellentesque adipiscing commodo elit at
                            imperdiet dui accumsan. Massa sed elementum tempus egestas sed sed risus pretium quam.
                        </p>
                        <p>
                            Sit amet mauris commodo quis imperdiet massa. In hendrerit gravida rutrum quisque non tellus
                            orci. Eu lobortis elementum nibh tellus molestie nunc non blandit massa. Molestie a iaculis
                            at erat. Nam aliquam sem et tortor consequat. Pharetra sit amet aliquam id diam. Urna nec
                            tincidunt praesent semper feugiat nibh sed. Nec ullamcorper sit amet risus nullam eget felis
                            eget nunc. Volutpat lacus laoreet non curabitur gravida. Arcu vitae elementum curabitur
                            vitae.
                        </p>
                        <p>
                            Porta nibh venenatis cras sed felis eget velit aliquet. Posuere urna nec tincidunt praesent
                            semper feugiat nibh sed. Nisl condimentum id venenatis a condimentum vitae. Turpis egestas
                            integer eget aliquet nibh. Tristique nulla aliquet enim tortor at auctor urna nunc id. Est
                            lorem ipsum dolor sit. Aliquet enim tortor at auctor urna nunc. Auctor urna nunc id cursus
                            metus aliquam eleifend. Non pulvinar neque laoreet suspendisse interdum. Erat velit
                            scelerisque in dictum. Suspendisse sed nisi lacus sed viverra tellus in hac. Turpis massa
                            sed elementum tempus egestas. Bibendum neque egestas congue quisque egestas diam in arcu
                            cursus.
                        </p>
                        <p>
                            Tellus mauris a diam maecenas sed enim ut sem. Ut sem viverra aliquet eget sit. Nec dui nunc
                            mattis enim ut tellus elementum sagittis vitae. Sit amet facilisis magna etiam tempor orci
                            eu lobortis. Accumsan lacus vel facilisis volutpat est velit egestas dui id. Non enim
                            praesent elementum facilisis leo. Vitae aliquet nec ullamcorper sit amet risus nullam eget.
                            Leo vel orci porta non pulvinar neque laoreet suspendisse. In ornare quam viverra orci
                            sagittis eu volutpat odio. Mi ipsum faucibus vitae aliquet nec ullamcorper sit. Dolor sit
                            amet consectetur adipiscing elit. Condimentum vitae sapien pellentesque habitant morbi
                            tristique senectus et. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu.
                        </p>
                        <p>
                            Blandit aliquam etiam erat velit scelerisque in dictum non. Eget sit amet tellus cras
                            adipiscing enim eu. Feugiat vivamus at augue eget arcu dictum varius duis at. Turpis egestas
                            pretium aenean pharetra magna ac placerat. Commodo nulla facilisi nullam vehicula ipsum a.
                            Felis donec et odio pellentesque. Sit amet mattis vulputate enim. Mi tempus imperdiet nulla
                            malesuada pellentesque elit eget gravida. Nisi est sit amet facilisis magna etiam tempor.
                            Rhoncus aenean vel elit scelerisque mauris pellentesque. Justo nec ultrices dui sapien eget
                            mi proin sed. Nunc mattis enim ut tellus elementum sagittis vitae. Vulputate mi sit amet
                            mauris commodo quis imperdiet massa. Eros in cursus turpis massa. Vitae semper quis lectus
                            nulla at volutpat diam ut. Eu volutpat odio facilisis mauris. Nulla at volutpat diam ut
                            venenatis tellus in metus. Pharetra vel turpis nunc eget lorem dolor. Mattis rhoncus urna
                            neque viverra justo. Amet nulla facilisi morbi tempus iaculis urna id volutpat. Nibh ipsum
                            consequat nisl vel. Pellentesque habitant morbi tristique senectus et netus et malesuada
                            fames. Nec feugiat in fermentum posuere urna nec tincidunt praesent.
                        </p>
                    </div>
                    <h1 className={styles.header}>Dritte Überschrift</h1>
                    <div className={styles.content}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Erat pellentesque adipiscing commodo elit at imperdiet.
                            Aliquam ut porttitor leo a diam sollicitudin tempor id. Integer feugiat scelerisque varius
                            morbi enim nunc. Molestie nunc non blandit massa enim nec dui. Augue eget arcu dictum varius
                            duis. Amet aliquam id diam maecenas ultricies. Erat pellentesque adipiscing commodo elit at
                            imperdiet dui accumsan. Massa sed elementum tempus egestas sed sed risus pretium quam.
                        </p>
                        <p>
                            Sit amet mauris commodo quis imperdiet massa. In hendrerit gravida rutrum quisque non tellus
                            orci. Eu lobortis elementum nibh tellus molestie nunc non blandit massa. Molestie a iaculis
                            at erat. Nam aliquam sem et tortor consequat. Pharetra sit amet aliquam id diam. Urna nec
                            tincidunt praesent semper feugiat nibh sed. Nec ullamcorper sit amet risus nullam eget felis
                            eget nunc. Volutpat lacus laoreet non curabitur gravida. Arcu vitae elementum curabitur
                            vitae.
                        </p>
                        <p>
                            Porta nibh venenatis cras sed felis eget velit aliquet. Posuere urna nec tincidunt praesent
                            semper feugiat nibh sed. Nisl condimentum id venenatis a condimentum vitae. Turpis egestas
                            integer eget aliquet nibh. Tristique nulla aliquet enim tortor at auctor urna nunc id. Est
                            lorem ipsum dolor sit. Aliquet enim tortor at auctor urna nunc. Auctor urna nunc id cursus
                            metus aliquam eleifend. Non pulvinar neque laoreet suspendisse interdum. Erat velit
                            scelerisque in dictum. Suspendisse sed nisi lacus sed viverra tellus in hac. Turpis massa
                            sed elementum tempus egestas. Bibendum neque egestas congue quisque egestas diam in arcu
                            cursus.
                        </p>
                        <p>
                            Tellus mauris a diam maecenas sed enim ut sem. Ut sem viverra aliquet eget sit. Nec dui nunc
                            mattis enim ut tellus elementum sagittis vitae. Sit amet facilisis magna etiam tempor orci
                            eu lobortis. Accumsan lacus vel facilisis volutpat est velit egestas dui id. Non enim
                            praesent elementum facilisis leo. Vitae aliquet nec ullamcorper sit amet risus nullam eget.
                            Leo vel orci porta non pulvinar neque laoreet suspendisse. In ornare quam viverra orci
                            sagittis eu volutpat odio. Mi ipsum faucibus vitae aliquet nec ullamcorper sit. Dolor sit
                            amet consectetur adipiscing elit. Condimentum vitae sapien pellentesque habitant morbi
                            tristique senectus et. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu.
                        </p>
                        <p>
                            Blandit aliquam etiam erat velit scelerisque in dictum non. Eget sit amet tellus cras
                            adipiscing enim eu. Feugiat vivamus at augue eget arcu dictum varius duis at. Turpis egestas
                            pretium aenean pharetra magna ac placerat. Commodo nulla facilisi nullam vehicula ipsum a.
                            Felis donec et odio pellentesque. Sit amet mattis vulputate enim. Mi tempus imperdiet nulla
                            malesuada pellentesque elit eget gravida. Nisi est sit amet facilisis magna etiam tempor.
                            Rhoncus aenean vel elit scelerisque mauris pellentesque. Justo nec ultrices dui sapien eget
                            mi proin sed. Nunc mattis enim ut tellus elementum sagittis vitae. Vulputate mi sit amet
                            mauris commodo quis imperdiet massa. Eros in cursus turpis massa. Vitae semper quis lectus
                            nulla at volutpat diam ut. Eu volutpat odio facilisis mauris. Nulla at volutpat diam ut
                            venenatis tellus in metus. Pharetra vel turpis nunc eget lorem dolor. Mattis rhoncus urna
                            neque viverra justo. Amet nulla facilisi morbi tempus iaculis urna id volutpat. Nibh ipsum
                            consequat nisl vel. Pellentesque habitant morbi tristique senectus et netus et malesuada
                            fames. Nec feugiat in fermentum posuere urna nec tincidunt praesent.
                        </p>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

export default Task1;
