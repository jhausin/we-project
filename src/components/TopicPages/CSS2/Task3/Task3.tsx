/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import styles from "./Task3.module.scss";

const Task3: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={value !== index} className={styles.container}>
            <div className={styles.grid}>
                <nav className={styles.nav}>
                    <a>The book series</a>
                    <a>Testimonials</a>
                    <a>The Author</a>
                    <a>Free resources</a>
                </nav>
                <main>
                    <h1>You don't know JavaScript</h1>
                    <div className={styles.contentArea}>
                        <div className={styles.imageWrapper}>
                            <img
                                alt='Complete JS Series'
                                src='https://kaul.inf.h-brs.de/we/assets/img/landing-img.png'
                            />
                        </div>
                        <div className={styles.text}>
                            <p>Don't just drift through javascript.</p>
                            <p>Understand how javascript works</p>
                            <p>Start your journey through the bumpy side of javascript</p>
                            <button>Order your copy now</button>
                        </div>
                    </div>
                </main>
                <footer>
                    <p>The first ebook in the book series is absolutely free.</p>
                    <button>Find out more about this offer</button>
                </footer>
            </div>
            ;
        </div>
    );
};

export default Task3;
