import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";

import styles from "./CSS2.module.scss";

import Task1 from "./Task1/Task1";
import Task2 from "./Task2/Task2";
import Task3 from "./Task3/Task3";

const CSS2: React.FC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange} scrollButtons='on' variant='scrollable'>
                    <Tab label='Flexbox' />
                    <Tab label='Grid' />
                    <Tab label='Webpage' />
                </Tabs>
            </AppBar>
            <Task1 value={value} index={0} />
            <Task2 value={value} index={1} />
            <Task3 value={value} index={2} />
        </div>
    );
};

export default CSS2;
