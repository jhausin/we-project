import React from "react";
import styles from "./Task2.module.scss";

const Task2: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={value !== index}>
            <div className={styles.grid}>
                <div className={styles.red}></div>
                <div className={styles.green}></div>
                <div className={styles.blue}></div>
                <div className={styles.pink}></div>
            </div>
        </div>
    );
};

export default Task2;
