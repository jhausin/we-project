import React from "react";
import styles from "./Task1.module.scss";

const Task1: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div className={styles.container} hidden={value !== index}>
            <div className={styles.red}></div>
            <div className={styles.flex}>
                <div className={styles.green}></div>
                <div className={styles.blue}></div>
                <div className={styles.pink}></div>
            </div>
        </div>
    );
};

export default Task1;
