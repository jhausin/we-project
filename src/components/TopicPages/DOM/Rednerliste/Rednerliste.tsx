import React, { useState } from "react";
import { TextField, Button, Typography } from "@material-ui/core";
import styles from "./Rednerliste.module.scss";

const Rednerliste: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    const [speaker, setSpeaker] = useState("");
    var runningClock: any = null;
    var clockInterval: any = null;
    const clock: any = {};

    const updateClock = (currentSpeaker: string) => {
        const el: HTMLElement = document
            .getElementById(currentSpeaker)!
            .getElementsByClassName("clock")[0] as HTMLElement;
        if (el) {
            el.innerText = formatClock(clock[currentSpeaker]);
        }
    };
    const runClock = (currentSpeaker: string) => {
        const button: HTMLElement = document
            .getElementById(currentSpeaker)!
            .getElementsByTagName("Button")[0] as HTMLElement;
        button.innerText = "Stopp!";
        runningClock = currentSpeaker;
        clockInterval = setInterval(() => {
            clock[currentSpeaker] += 1;
            updateClock(currentSpeaker);
        }, 1000);
    };

    const stopAllClocks = () => {
        runningClock = null;
        clearInterval(clockInterval);
        const elements: HTMLCollectionOf<HTMLButtonElement> = document
            .getElementById("list")!
            .getElementsByTagName("button");
        [...elements].forEach((el) => {
            el.innerText = "Start!";
        });
    };
    const addListElement = (name: string) => {
        const list = document.getElementById("list")!;
        const li = document.createElement("li")!;
        li.id = name;
        li.innerHTML = "<span>" + name + "</span> <span class='clock'>00:00:00</span><button>Stop!</button>";
        list.append(li);
        document
            .getElementById(name)!
            .getElementsByTagName("button")[0]
            .addEventListener("click", () => buttonHandler(name));
    };
    const buttonHandler = (whichClock: string) => {
        const runningClock_ = runningClock;
        stopAllClocks();
        if (runningClock_ !== whichClock) runClock(whichClock);
    };
    const handleSubmit = (event: { preventDefault: () => void }) => {
        event.preventDefault();
        const currentSpeaker: string = speaker;
        clock[currentSpeaker] = 0;
        addListElement(currentSpeaker);
        stopAllClocks();
        runClock(currentSpeaker);
    };
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setSpeaker(event.target.value as string);
    };
    const formatClock = (clock: number) => {
        const hours = Math.floor(clock / 3600);
        const remainder = clock % 3600;
        const minutes = Math.floor(remainder / 60);
        const seconds = remainder % 60;
        return (
            hours.toLocaleString("de", { minimumIntegerDigits: 2 }) +
            ":" +
            minutes.toLocaleString("de", { minimumIntegerDigits: 2 }) +
            ":" +
            seconds.toLocaleString("de", { minimumIntegerDigits: 2 })
        );
    };
    return (
        <div hidden={index !== value} className={styles.container}>
            <Typography variant='h4' align='center' gutterBottom>
                Rednerliste
            </Typography>
            <form id='form' className={styles.form}>
                <TextField id='input' variant='outlined' onChange={handleChange} label='Neuer Redner' />
                <Button variant='contained' color='primary' onClick={handleSubmit} style={{ maxWidth: "150px" }}>
                    Hinzufügen
                </Button>
            </form>
            <ul id='list' className={styles.list}></ul>
        </div>
    );
};

export default Rednerliste;
