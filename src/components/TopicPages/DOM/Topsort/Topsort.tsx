import React, { useState } from "react";
import { Typography, Divider, Button, TextField } from "@material-ui/core";

import styles from "./Topsort.module.scss";
const Topsort: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    const [result, setResult] = useState<string[]>([]);
    const [dependency, setDependency] = useState(["", ""]);
    const [dependencies, setDependencies] = useState([] as any);

    const handleSortClick = () => {
        topsort(dependencies);
    };
    const handleChange = (event: React.ChangeEvent<{ value: string }>) => {
        const dependency = event.target.value;
        setDependency(dependency.replaceAll(/\s+/g, "").split(",")); //replacing whitespaces
    };
    const handleResetClick = () => {
        setDependencies([] as any);
        setResult([]);
    };
    const handleAddClick = () => {
        const deps = [...dependencies];
        deps.push(dependency);
        setDependencies(deps);
    };
    const topsort = (arr: string[][]) => {
        const hasNoDependency = (el: string, arr: string[][]) => {
            return !arr.some((deps) => deps.length > 1 && deps[1] === el);
        };
        var sort: string[] = [];
        while (arr.length > 0) {
            // eslint-disable-next-line no-loop-func
            arr.forEach((deps) => {
                if (hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    arr = arr.map((d) => d.filter((task) => task !== deps[0]));
                    arr = arr.filter((d) => d.length > 0);
                }
            });
        }
        setResult([...new Set(sort)]);
    };

    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <Typography variant='h4' align='center' gutterBottom>
                    Topologische Sortierung
                </Typography>
                <div className={styles.content}>
                    <Typography variant='h5' gutterBottom>
                        Input:
                    </Typography>
                    <div className={styles.input}>
                        <TextField
                            label='dep1, dep2'
                            variant='outlined'
                            onChange={handleChange}
                            className={styles.inputField}
                        />
                        <Button variant='contained' color='primary' onClick={handleAddClick} className={styles.add}>
                            Hinzufügen
                        </Button>
                        <Button variant='contained' color='secondary' onClick={handleResetClick}>
                            Reset
                        </Button>
                    </div>
                    <Typography variant='body1' color='textSecondary' gutterBottom>
                        {dependencies.length > 0 ? JSON.stringify(dependencies) : ""}
                    </Typography>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={handleSortClick}
                        style={{ margin: "10px 0px" }}
                    >
                        Sort
                    </Button>
                    <Divider style={{ margin: "20px" }} />
                    <Typography variant='h5' gutterBottom>
                        Output:
                    </Typography>
                    <Typography variant='body1' color='textSecondary'>
                        {result.length > 0 ? JSON.stringify(result) : ""}
                    </Typography>
                </div>
            </div>
        </div>
    );
};

export default Topsort;
