import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";

import Performance from "./Performance/Performance";
import Rednerliste from "./Rednerliste/Rednerliste";
import Topsort from "./Topsort/Topsort";
import styles from "./DOM.module.scss";

const DOM: React.FC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange} scrollButtons='on' variant='scrollable'>
                    <Tab label='Performance' />
                    <Tab label='Rednerliste' />
                    <Tab label='Topsort Webapp' />
                </Tabs>
            </AppBar>
            <Performance value={value} index={0} />
            <Rednerliste value={value} index={1} />
            <Topsort value={value} index={2} />
        </div>
    );
};

export default DOM;
