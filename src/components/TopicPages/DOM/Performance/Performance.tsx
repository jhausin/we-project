/* eslint-disable @typescript-eslint/no-unused-expressions */
import React, { useEffect } from "react";
import { TableContainer, Table, TableBody, TableHead, TableRow, TableCell, Typography } from "@material-ui/core";
import styles from "./Performance.module.scss";

const Performance: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    useEffect(() => {
        const innerHTML: HTMLElement = document.getElementById("innerHTML")!;
        const innerText: HTMLElement = document.getElementById("innerText")!;
        const textContent: HTMLElement = document.getElementById("textContent")!;
        const outerHTML: HTMLElement = document.getElementById("outerHTML")!;

        innerHTML.innerHTML = perfomanceTest(innerHTML, 10000, "innerHTML").toString();
        innerText.innerHTML = perfomanceTest(innerText, 10000, "innerText").toString();
        textContent.innerHTML = perfomanceTest(textContent, 10000, "textContent").toString();
        outerHTML.innerHTML = perfomanceTest(outerHTML, 10000, "outerHTML").toString();
    });
    const perfomanceTest = (element: HTMLElement, count: number, method: string): number => {
        var startTime: number;
        switch (method) {
            case "innerHTML": {
                startTime = performance.now();
                for (var i = 0; i < count; i++) {
                    element.innerHTML;
                }
                return (performance.now() - startTime) / count;
            }
            case "innerText": {
                startTime = performance.now();
                for (var j = 0; j < count; j++) {
                    element.innerText;
                }
                return (performance.now() - startTime) / count;
            }
            case "textContent": {
                startTime = performance.now();
                for (var k = 0; k < count; k++) {
                    element.textContent;
                }
                return (performance.now() - startTime) / count;
            }
            default: {
                startTime = performance.now();
                for (var l = 0; l < count; l++) {
                    element.outerHTML;
                }
                return (performance.now() - startTime) / count;
            }
        }
    };
    return (
        <div hidden={index !== value} className={styles.container}>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell colSpan={12} align='center' variant='head'>
                                <Typography variant='h4'>Perfomancemessung</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className={styles.tableBody}>
                        <TableRow>
                            <TableCell colSpan={3} align='center'>
                                innerHTML
                            </TableCell>
                            <TableCell colSpan={3} align='center'>
                                innerText
                            </TableCell>
                            <TableCell colSpan={3} align='center'>
                                textContent
                            </TableCell>
                            <TableCell colSpan={3} align='center'>
                                outerHTML
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3} align='center' id='innerHTML'>
                                0
                            </TableCell>
                            <TableCell colSpan={3} align='center' id='innerText'>
                                0
                            </TableCell>
                            <TableCell colSpan={3} align='center' id='textContent'>
                                0
                            </TableCell>
                            <TableCell align='center' id='outerHTML'>
                                123
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default Performance;
