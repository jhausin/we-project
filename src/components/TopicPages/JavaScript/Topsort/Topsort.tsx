import React, { useState } from "react";
import { Typography, Divider, Button } from "@material-ui/core";

import styles from "./Topsort.module.scss";
const Topsort: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    const [result, setResult] = useState<string[]>([]);
    const dependencies: string[][] = [
        ["essen", "studieren"],
        ["schlafen", "studieren"],
        ["studieren", "prüfen"],
    ];
    const handleClick = () => {
        topsort(dependencies);
    };

    const topsort = (arr: string[][]) => {
        const hasNoDependency = (el: string, arr: string[][]) => {
            return !arr.some((deps) => deps.length > 1 && deps[1] === el);
        };
        var sort: string[] = [];
        while (arr.length > 0) {
            // eslint-disable-next-line no-loop-func
            arr.forEach((deps) => {
                if (hasNoDependency(deps[0], arr)) {
                    sort.push(deps[0]);
                    arr = arr.map((d) => d.filter((task) => task !== deps[0]));
                    arr = arr.filter((d) => d.length > 0);
                }
            });
        }
        setResult(sort);
    };

    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <Typography variant='h4' align='center' gutterBottom>
                    Topologische Sortierung
                </Typography>
                <div className={styles.content}>
                    <Typography variant='h5' gutterBottom>
                        Input:
                    </Typography>
                    <Typography variant='body1' color='textSecondary' gutterBottom>
                        {JSON.stringify(dependencies)}
                    </Typography>
                    <Button variant='contained' color='primary' onClick={handleClick} style={{ margin: "10px 0px" }}>
                        Sort
                    </Button>

                    <Divider style={{ margin: "20px" }} />
                    <Typography variant='h5' gutterBottom>
                        Output:
                    </Typography>
                    <Typography variant='body1' color='textSecondary'>
                        {result.length > 0 ? JSON.stringify(result) : ""}
                    </Typography>
                </div>
            </div>
        </div>
    );
};

export default Topsort;
