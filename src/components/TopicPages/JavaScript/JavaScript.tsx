import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";

import styles from "./JavaScript.module.scss";
import Fibonacci from "./Fibonacci/Fibonacci";
import Topsort from "./Topsort/Topsort";

const JavaScript: React.FC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange}>
                    <Tab label='Fibonacci' />
                    <Tab label='Topsort' />
                </Tabs>
            </AppBar>
            <Fibonacci value={value} index={0} />
            <Topsort value={value} index={1} />
        </div>
    );
};

export default JavaScript;
