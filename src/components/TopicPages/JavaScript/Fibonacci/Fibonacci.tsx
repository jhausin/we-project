import React, { useState } from "react";
import { Typography, TextField, Button, Snackbar } from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import CountUp from "react-countup";

import styles from "./Fibonacci.module.scss";
const Fibonacci: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    const [position, setPosition] = useState(0);
    const [result, setResult] = useState(-1);
    const [error, setError] = useState(false);

    const handlePositionChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        if ((event.target.value as number) < 0) {
            setError(true);
        } else {
            setError(false);
            setPosition(event.target.value as number);
        }
    };
    const handleClick = (event: React.ChangeEvent<{}>) => {
        setResult(fibonacci(position));
    };
    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setError(false);
    };
    const Alert = (props: AlertProps) => {
        return <MuiAlert elevation={6} variant='filled' {...props} />;
    };
    const fibonacci = (position: number): number => {
        if (position < 1) {
            return table[0];
        } else if (table[position] !== undefined) {
            return table[position];
        }
        var value: number = fibonacci(position - 1) + fibonacci(position - 2);
        table[position] = value;
        return value;
    };
    var table: any = {
        0: 0n,
        1: 1n,
    };
    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <Typography variant='h4' align='center'>
                    Fibonacci Calculator
                </Typography>
                <Typography variant='caption' color='textSecondary' style={{ padding: "20px" }} gutterBottom>
                    Die Fibonacci-Folge ist eine unendliche Folge von Zahlen, bei der sich die jeweils folgende Zahl
                    durch Addition ihrer beiden vorherigen Zahlen ergibt.
                </Typography>
                <div className={styles.inputContainer}>
                    <TextField
                        className={styles.input}
                        variant='outlined'
                        label='Gib eine Nummer ein'
                        type='number'
                        onChange={handlePositionChange}
                    />
                    <Button variant='contained' color='primary' onClick={handleClick} className={styles.button}>
                        Berechnen
                    </Button>
                </div>
                <div hidden={result < 0}>
                    <Typography variant='h6' align='center' style={{ margin: "10px 0" }}>
                        Ergebnis: <CountUp start={0} end={result} duration={1} />
                    </Typography>
                </div>
                <Snackbar autoHideDuration={3000} open={error} onClose={handleClose}>
                    <Alert className={styles.alert} severity='error'>
                        Nur positive Zahlen sind erlaubt.
                    </Alert>
                </Snackbar>
            </div>
        </div>
    );
};

export default Fibonacci;
