import React, { useState } from "react";
import { Typography, Button } from "@material-ui/core";

import styles from "./Fetch.module.scss";

const Fetch: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    const [textA, setTextA] = useState("");
    const [textB, setTextB] = useState("");
    const [changedText, setChangedText] = useState("");
    const UrlA: string = "http://www2.inf.h-brs.de/~jhausi2s/A.txt";
    const UrlB: string = "http://www2.inf.h-brs.de/~jhausi2s/B.txt";

    const handlePromiseClick = () => {
        fetchText("promise");
    };
    const handleAsyncClick = () => {
        fetchText();
    };
    const handleReset = () => {
        setTextA("");
        setTextB("");
        setChangedText("");
    };
    const fetchText = async (method: string = "async") => {
        if (method === "promise") {
            setTextA(await fetch(UrlA).then((res) => res.text()));
            setTextB(await fetch(UrlB).then((res) => res.text()));
            concatText(textA, textB);
        } else {
            Promise.all([fetch(UrlA).then((resA) => resA.text()), fetch(UrlB).then((resB) => resB.text())]).then(
                ([resA, resB]) => {
                    setTextA(resA);
                    setTextB(resB);
                    concatText(textA, textB);
                },
            );
        }
    };
    const concatText = (textA: string, textB: string) => {
        let currA: string;
        let currB: string;
        let strConcat: string = "";
        const linesA: string[] = textA.split("\n");
        const linesB: string[] = textB.split("\n");
        while (linesA.length && linesB.length) {
            currA = linesA.shift()!;
            currB = linesB.shift()!;
            strConcat +=
                " " +
                currA.substring(0, Math.floor(currA.length / 2)) +
                currB.substring(Math.floor(currB.length / 2), currB.length);
        }
        if (linesB.length) {
            linesB.forEach((line) => (strConcat += line));
        } else if (linesA.length) {
            linesA.forEach((line) => (strConcat += " " + line));
        }
        setChangedText(strConcat);
    };
    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <Typography variant='h4' align='center' gutterBottom>
                    Promise & Async/Await
                </Typography>
                <div className={styles.buttons}>
                    <Button color='primary' variant='contained' onClick={handlePromiseClick}>
                        Fetch mit Promise
                    </Button>
                    <Button color='primary' variant='contained' onClick={handleAsyncClick}>
                        Fetch mit async/await
                    </Button>
                </div>
                <div style={{ padding: "0 2%" }}>
                    <Typography variant='h5' align='center' gutterBottom>
                        Text A:
                    </Typography>
                    <Typography align='center' gutterBottom>
                        {textA}
                    </Typography>
                    <Typography variant='h5' align='center' gutterBottom>
                        Text B:
                    </Typography>
                    <Typography align='center' gutterBottom>
                        {textB}
                    </Typography>
                    <Typography variant='h5' align='center' gutterBottom>
                        Concatenated Text:
                    </Typography>
                    <Typography id='changedText' align='center' gutterBottom>
                        {changedText}
                    </Typography>
                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <Button
                            variant='contained'
                            color='secondary'
                            id='reset'
                            style={{ maxWidth: "200px", margin: "5% 0" }}
                            onClick={handleReset}
                        >
                            Reset
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Fetch;
