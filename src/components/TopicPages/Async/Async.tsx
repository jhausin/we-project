import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";

import Fetch from "./Fetch/Fetch";
import Navigator from "./Navigator/Navigator";

import styles from "./Async.module.scss";

const Async: React.FC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange}>
                    <Tab label='Promises & async/await' />
                    <Tab label='WWW-Navigator' />
                </Tabs>
            </AppBar>
            <Fetch value={value} index={0} />
            <Navigator value={value} index={1} />
        </div>
    );
};

export default Async;
