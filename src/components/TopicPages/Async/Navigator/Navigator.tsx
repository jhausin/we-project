import React, { useState, useEffect } from "react";
import { Typography, Button } from "@material-ui/core";
import styles from "./Navigator.module.scss";
import jsonData from "./navigator_content.json";

interface Data {
    [x: string]: any;
}

const Navigator: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    const [data, setData] = useState<Data>({});

    const sidebar: HTMLElement = document.getElementById("sidebar")!;
    const content: HTMLElement = document.getElementById("content")!;
    const reference: HTMLElement = document.getElementById("reference")!;

    let topicContent: { [x: string]: { references: string; content: string } };

    useEffect(() => {
        setData(jsonData);
    }, []);

    const handleHTMLButtonClick = () => {
        createSidebar("html");
    };
    const handleCSSButtonClick = () => {
        createSidebar("css");
    };
    const handleJavascriptButtonClick = () => {
        createSidebar("javascript");
    };
    const createSidebar = (topic: string | number) => {
        sidebar.innerHTML = "";
        const menuData: string[] = data[topic];
        topicContent = data[topic];
        for (let key in menuData) {
            const buttonElement: HTMLButtonElement = document.createElement("button");
            buttonElement.innerHTML = key.toUpperCase();
            buttonElement.onclick = () => displayContent(key);
            sidebar.appendChild(buttonElement);
        }
    };

    const displayContent = (topic: string) => {
        content.innerHTML = `<h2>${topic.toUpperCase()}</h2>`;
        const p: HTMLParagraphElement = document.createElement("p");
        p.innerHTML = topicContent[topic]!.content;
        content.appendChild(p);
        reference.innerHTML = topicContent[topic].references;
    };
    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <div className={styles.navbar}>
                    <Typography variant='h4' align='center' gutterBottom>
                        WWW-Navigator
                    </Typography>
                    <Button id='html' color='primary' variant='contained' onClick={handleHTMLButtonClick}>
                        HTML
                    </Button>
                    <Button id='css' color='primary' variant='contained' onClick={handleCSSButtonClick}>
                        CSS
                    </Button>
                    <Button id='javascript' color='primary' variant='contained' onClick={handleJavascriptButtonClick}>
                        JavaScript
                    </Button>
                </div>
                <div id='sidebar' className={styles.sidebar}></div>
                <div id='content' className={styles.content}></div>
                <div id='reference' className={styles.source}></div>
            </div>
        </div>
    );
};
export default Navigator;
