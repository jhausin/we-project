export const eingabe: string = `<template>
<input v-model="message" />
<p>
  Anzahl Buchstaben: {{letters}}
</p>
<p>
  Anzahl Leerzeichen: {{whitespaces}}
</p>
<p>
  Anzahl Worte: {{words}}
</p>
</template>
<script>
export default {
name: 'HelloWorld',
data() {
  return {
    message: "",
  }
},
computed: {
  words() {
    const arr = this.message.split(" ");
    return arr.filter(el => el !== " " && el !== "").length
  },
  whitespaces() {
    return [...this.message].filter(l => l === " ").length
  },
  letters() {
    return this.message.length
  },
},
props: {
  msg: String
}
}
</script>
<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
h3 {
margin: 40px 0 0;
}
ul {
list-style-type: none;
padding: 0;
}
li {
display: inline-block;
margin: 0 10px;
}
a {
color: #42b983;
}
</style>`;

export const menuComponent: string = `<template>
<div :class="mode" class="menu">
  <div class="menu-item" :key="link.href" v-for="link in links">
    <a :href="link.href">{{link.title}}</a>
  </div>
</div>
</template>
<script>
export default {
name: 'Menu',
data() {
  return {
    message: "",
  }
},
props: {
  mode: {
    type: String,
    default: "horizontal"
  },
  links: {
    type: Array,
    default: () => [],
  }
}
}
</script>
<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.menu {
}
.menu.horizontal {
  display: flex;
  justify-content: space-between;
}
.menu-item {
  background-color: #eeee;
  flex-grow: 1;
  padding: 5px 10px;
}
a {
  text-decoration: none;
}
</style>`;

export const css: string = `<template>
<SubPage>
  <template v-slot:sidebar>
    <Sidebar :on-click="handleClick" :links="links" />
  </template>
  <template v-slot:content>
    <Main :content="this.content" />
  </template>
  <template v-slot:references>
    <Reference :reference="reference" />
  </template>
</SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
name: 'CSS',
components: { Reference, SubPage, Sidebar, Main },
data() {
  return {
    data: {},
    content: "",
    reference: "",
    links: [],
  }
},
methods: {
  handleClick(link) {
    this.content = this.data[link].content;
    this.reference = this.data[link].references[0];
    this.links = Object.keys(this.data).map((item) => {
      return {
        title: item,
        active: link === item,
      }
    })
  }
},
async mounted () {
  const res = await fetchData("css");
  this.data = res;
  this.links = Object.keys(res).map((item, index) => {
    return {
      title: item,
      active: index === 0,
    }
  })
  this.content = res[this.links[0].title].content;
  this.reference = res[this.links[0].title].references[0];
  console.log(this.content, this.reference);
}
}
</script>
`;

export const html: string = `<template>
<SubPage>
  <template v-slot:sidebar>
    <Sidebar :on-click="handleClick" :links="links" />
  </template>
  <template v-slot:content>
    <Main :content="this.content" />
  </template>
  <template v-slot:references>
    <Reference :reference="reference" />
  </template>
</SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
name: 'HTML',
components: { Reference, SubPage, Sidebar, Main },
data() {
  return {
    data: {},
    content: "",
    reference: "",
    links: [],
  }
},
methods: {
  handleClick(link) {
    this.content = this.data[link].content;
    this.reference = this.data[link].references[0];
    this.links = Object.keys(this.data).map((item) => {
      return {
        title: item,
        active: link === item,
      }
    })
  }
},
async mounted () {
  console.log(this.filename)
  const res = await fetchData("html");
  this.data = res;
  this.links = Object.keys(res).map((item, index) => {
    return {
      title: item,
      active: index === 0,
    }
  })
  this.content = res[this.links[0].title].content;
  this.reference = res[this.links[0].title].references[0];
}
}
</script>`;

export const javascript: string = `<template>
<SubPage>
  <template v-slot:sidebar>
    <Sidebar :on-click="handleClick" :links="links" />
  </template>
  <template v-slot:content>
    <Main :content="this.content" />
  </template>
  <template v-slot:references>
    <Reference :reference="reference" />
  </template>
</SubPage>
</template>

<script>
import SubPage from "@/components/SubPage";
import Sidebar from "@/components/Sidebar";
import { fetchData } from "@/services/fetchData";
import Reference from "@/components/Reference";
import Main from "@/components/Main";
export default {
name: 'JavaScript',
components: { Reference, SubPage, Sidebar, Main },
data() {
  return {
    data: {},
    content: "",
    reference: "",
    links: [],
  }
},
methods: {
  handleClick(link) {
    this.content = this.data[link].content;
    this.reference = this.data[link].references[0];
    this.links = Object.keys(this.data).map((item) => {
      return {
        title: item,
        active: link === item,
      }
    })
  }
},
async mounted () {
  const res = await fetchData("javascript");
  this.data = res;
  this.links = Object.keys(res).map((item, index) => {
    return {
      title: item,
      active: index === 0,
    }
  })
  this.content = res[this.links[0].title].content;
  this.reference = res[this.links[0].title].references[0];
  console.log(this.content, this.reference);
}
}
</script>`;

export const vueApp: string = `<template>
<Page>
  <template v-slot:header>
    <Title title="WWW-Navigator" />
    <Navigation v-bind:links="links" />
  </template>
  <template v-slot:main>
    <router-view/>
  </template>
</Page>
</template>

<style>
#app {
font-family: Avenir, Helvetica, Arial, sans-serif;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
text-align: center;
color: #2c3e50;
}

#nav {
padding: 30px;
}

#nav a {
font-weight: bold;
color: #2c3e50;
}

#nav a.router-link-exact-active {
color: #42b983;
}
</style>
<script>
import Page from "@/components/Page";
import Navigation from "@/components/Navigation";
import Title from "@/components/Title";
import store from "./store";
export const links =  [
{
  href: "/html",
  title: "HTML"
},
{
  href: "/css",
  title: "CSS"
},
{
  href: "/javascript",
  title: "JavaScript"
}
]
export default {
components: { Page, Navigation, Title },
store,
mounted() {
  store.dispatch("fetchData", "html");
},
data() {
  return {
    links,
  }
}
}
</script>
`;
export const mainjs: string = `import { createApp } from 'vue'
import router from './router'
import App from "./App";
import store from './store'

createApp(App).use(store).use(router).mount('#app')
`;
export const indexjs: string = `import { createStore } from 'vuex'

export default createStore({
  state: {
    data: {}
  },
  mutations: {
    newData(state, payload) {
      state.data = payload;
    },
  },
  actions: {
    async fetchData({commit}, fileName) {
      // ...
      const response = await fetch("http://www2.inf.h-brs.de/~tbasti2s/" + fileName)
      commit("newData", await response.json())
    }
  },
})

fetchData: 
export const fetchData = async (fileName) => {
  const response = await fetch("http://www2.inf.h-brs.de/~tbasti2s/" + fileName)
  return response.json()
}

router/index.js : 
import { createRouter, createWebHistory } from 'vue-router'
import HTML from "@/views/HTML";
import CSS from "@/views/CSS";
import JavaScript from "@/views/JavaScript";

const routes = [
  {
    path: '/html',
    name: 'HTML',
    component: HTML,
  },
  {
    path: '/css',
    name: 'CSS',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: CSS
  },
  {
    path: "/javascript",
    name: "JavaScript",
    component: JavaScript
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
`;

export const subpage: string = `<template>
<div id="subpage">
  <div class="sidebar">
    <slot name="sidebar"/>
  </div>
  <div class="content">
  <slot name="content"/>
  </div>
  <div class="references">
    <slot name="references"/>
  </div>
</div>
</template>

<script>
export default {
name: 'SubPage',
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
#subpage {
display: flex;
}
.sidebar, .references  {
flex: 1;
padding: 10px;
text-align: left;
}
.references {
text-align: left!important;
}
.content {
padding: 10px;
flex: 3;
text-align: left;
}

</style>
`;

export const sidebar: string = `<template>
<div>
  <h3>Navigation</h3>
  <span @click="onClick(link.title)" :key="link.title" v-for="link in links">{{link.title}}</span>
</div>
</template>

<script>
export default {
name: 'Sidebar',
props: {
  links: Array,
  onClick: Function,
}
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
span {
  display: block;
  cursor: pointer;
}
</style>
`;

export const reference: string = `<template>
<div>
  <h3>Quellen:</h3>
  <a :href="reference">{{reference}}</a>
</div>
</template>

<script>
export default {
name: 'Reference',
props: {
  reference: String,
}
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
a {
  word-break: break-all;
  text-align: left!important;
}
</style>
`;
export const page: string = `<template>
<div id="page">
  <header>
    <slot name="header" />
  </header>
  <div id="content">
    <main>
      <slot name="main"/>
    </main>
  </div>
</div>
</template>

<script>
export default {
name: 'Page',
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>
`;
export const navigation: string = `<template>
<div class="navigation">
  <div v-for="link in links" :key="link.key">
    <router-link
      :to="link.href"
      >
      {{ link.title }}
    </router-link>
  </div>
</div>
</template>

<script>

export default {
name: 'Navigation',
props: {
  links: Array,
  default: () => [],
}
}
</script>

<style scoped>
.navigation {
display: flex;
}
.navigation > div {
padding: 5px 10px;
}

</style>`;

export const main: string = `<template>
<div>
  <h3>Erklärung:</h3>
  {{content}}
</div>
</template>

<script>
export default {
name: 'Main',
props: {
  content: String
}
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>

`;
