import React from "react";
import { Typography } from "@material-ui/core";

import styles from "./Info.module.scss";

const Info: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    return (
        <div hidden={index !== value}>
            <div className={styles.container}>
                <Typography variant='h4'>Vorwort</Typography>
                <Typography variant='body1' color='textSecondary' style={{ margin: "2% 5%" }}>
                    In der Übung wurde Vue.js behandelt, da ich aber das Framework React.js verwende ist es mir nicht
                    möglich die Komponenten mit Vue.js darzustellen. Ich habe versucht mit dem "Vue-React" Modul Vue
                    Komponenten in React zu importieren, jedoch hat es nach viel rumprobieren nicht funktioniert. In den
                    oben zusehenden Tabs lassen sich die Quellcodes der jeweiligen Komponenten anzeigen.
                    <br /> Der WWW-Navigator kann trotzdem in Übung 10 betrachtet werden, ist jedoch als React Component
                    erstellt worden.
                </Typography>
            </div>
        </div>
    );
};

export default Info;
