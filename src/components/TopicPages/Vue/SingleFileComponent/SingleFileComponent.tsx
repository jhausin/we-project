import React from "react";
import { Typography } from "@material-ui/core";
import styles from "./SingleFileComponent.module.scss";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { eingabe } from "../sourcecodes";
const SingleFileComponent: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={index !== value}>
            <div className={styles.container}>
                <Typography variant='h4' gutterBottom>
                    Singe File Component
                </Typography>

                <div>
                    <Typography variant='h6' color='textSecondary'>
                        Eingabe.vue
                    </Typography>
                    <SyntaxHighlighter language='javascript' style={a11yLight}>
                        {eingabe}
                    </SyntaxHighlighter>
                </div>
            </div>
        </div>
    );
};

export default SingleFileComponent;
