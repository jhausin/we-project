import React from "react";
import { Typography } from "@material-ui/core";
import { menuComponent } from "../sourcecodes";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import styles from "./MenuComponent.module.scss";

const MenuComponent: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={index !== value}>
            <div className={styles.container}>
                <Typography variant='h4' gutterBottom>
                    Menu Component
                </Typography>

                <div>
                    <Typography variant='h6' color='textSecondary'>
                        Eingabe.vue
                    </Typography>
                    <SyntaxHighlighter language='javascript' style={a11yLight}>
                        {menuComponent}
                    </SyntaxHighlighter>
                </div>
            </div>
        </div>
    );
};

export default MenuComponent;
