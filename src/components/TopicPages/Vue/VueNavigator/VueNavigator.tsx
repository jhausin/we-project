import React from "react";
import { Typography } from "@material-ui/core";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";

import {
    css,
    html,
    javascript,
    vueApp,
    mainjs,
    indexjs,
    subpage,
    sidebar,
    reference,
    page,
    navigation,
    main,
} from "../sourcecodes";
import styles from "./VueNavigator.module.scss";

const VueNavigator: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { value, index } = props;
    return (
        <div hidden={value !== index}>
            <div className={styles.container}>
                <Typography variant='h4' gutterBottom>
                    Vue Navigator
                </Typography>
                <div className={styles.sourcecodes}>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            CSS.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {css}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            HTML.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {html}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            JavaScript.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {javascript}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            App.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {vueApp}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            CSS.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {css}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            main.js
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {mainjs}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            index.js
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {indexjs}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            SubPage.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {subpage}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            Sidebar.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {sidebar}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            Reference.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {reference}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            Page.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {page}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            Navigation.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {navigation}
                        </SyntaxHighlighter>
                    </div>
                    <div>
                        <Typography variant='h6' color='textSecondary'>
                            Main.vue
                        </Typography>
                        <SyntaxHighlighter language='javascript' style={a11yLight}>
                            {main}
                        </SyntaxHighlighter>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default VueNavigator;
