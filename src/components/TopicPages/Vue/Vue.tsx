import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";
import styles from "./Vue.module.scss";

import MenuComponent from "./MenuComponent/MenuComponent";
import SingleFileComponent from "./SingleFileComponent/SingleFileComponent";
import VueNavigator from "./VueNavigator/VueNavigator";
import Info from "./Info/Info";
const Vue: React.FC = () => {
    const [value, setValue] = useState(0);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange} scrollButtons='on' variant='scrollable'>
                    <Tab label='Info' />
                    <Tab label='Single File Component' />
                    <Tab label='Menu Component' />
                    <Tab label='WWW-Navigator' />
                </Tabs>
            </AppBar>
            <Info value={value} index={0} />
            <SingleFileComponent value={value} index={1} />
            <MenuComponent value={value} index={2} />
            <VueNavigator value={value} index={3} />
        </div>
    );
};

export default Vue;
