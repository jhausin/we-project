import { Typography, Grid, List } from "@material-ui/core";
import React from "react";
import ListItem from "./ListItem/ListItem";
import styles from "./Introduction.module.scss";
import FounderTable from "./FounderTable/FounderTable";
import Profiles from "./Profiles/Profiles";

const Introduction: React.FC = () => {
    return (
        <div className={styles.container}>
            <Grid container spacing={3} justify='center'>
                <Grid item xs={12} sm={12} md={6} lg={6} className={styles.content}>
                    <Typography align='center' variant='h3'>
                        Inventors of the Web
                    </Typography>
                    <List>
                        <ListItem
                            itemName='Tim Berners-Lee'
                            text=' WWW, HTTP, HTML, URI'
                            imageURL='https://kaul.inf.h-brs.de/we/assets/img/tbl.jpg'
                        />
                        <ListItem
                            itemName='Brendan Eich'
                            imageURL='https://kaul.inf.h-brs.de/we/assets/img/eich.jpg'
                            text='JavaScript'
                        />
                        <ListItem itemName='Hakom Lie und Bert Bos' text='CSS' />
                    </List>
                    <FounderTable />
                    <Profiles />
                </Grid>
            </Grid>
        </div>
    );
};

export default Introduction;
