import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Typography } from "@material-ui/core";
import React from "react";

const FounderTable: React.FC = () => {
    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={12} align='center' variant='head'>
                            <Typography variant='h4'>Inventors of the WWW</Typography>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell colSpan={3} align='center'>
                            WWW
                        </TableCell>
                        <TableCell colSpan={3} align='center'>
                            HTML
                        </TableCell>
                        <TableCell colSpan={3} align='center'>
                            CSS
                        </TableCell>
                        <TableCell colSpan={3} align='center'>
                            JavaScript
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell colSpan={6} align='center'>
                            Tim Berners-Lee
                        </TableCell>
                        <TableCell colSpan={3} align='center'>
                            Hakon Lie und Bert Bos
                        </TableCell>
                        <TableCell colSpan={3} align='center'>
                            Brendan Eich
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default FounderTable;
