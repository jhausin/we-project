import React from "react";
import { ListItem, ListItemAvatar, ListItemText, Typography, Avatar } from "@material-ui/core";

interface ItemProps {
    itemName: string;
    imageURL?: string;
    text: string;
}

const Item: React.FC<ItemProps> = (props: ItemProps) => {
    const { itemName, imageURL, text } = props;
    return (
        <ListItem alignItems='flex-start'>
            <ListItemAvatar>
                <Avatar alt={itemName} src={imageURL ? imageURL : " "} />
            </ListItemAvatar>
            <ListItemText
                primary={itemName}
                secondary={
                    <React.Fragment>
                        <Typography component='span' variant='body2' color='textSecondary'>
                            {text}
                        </Typography>
                    </React.Fragment>
                }
            />
        </ListItem>
    );
};

export default Item;
