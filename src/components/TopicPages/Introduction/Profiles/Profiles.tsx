import { Grid, Card, CardContent, Typography, CardMedia } from "@material-ui/core";
import React from "react";
import tbl from "../../../../assets/TimBernersLee.jpeg";
import hl from "../../../../assets/HakonLie.jpg";
import bb from "../../../../assets/BertBos.jpg";
import be from "../../../../assets/BrendanEich.jpg";
const Profile = () => {
    return (
        <Grid container spacing={2} style={{ marginTop: "10px" }}>
            <Grid item xs={6} sm={6} lg={3}>
                <Card>
                    <CardContent>
                        <CardMedia style={{ height: "120px", borderRadius: "5px" }} image={tbl} />
                        <Typography variant='caption' color='textSecondary'>
                            Tim Berners Lee
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6} sm={6} lg={3}>
                <Card>
                    <CardContent>
                        <CardMedia style={{ height: "120px", borderRadius: "5px" }} image={hl} />
                        <Typography variant='caption' color='textSecondary'>
                            Hakon Lie
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6} sm={6} lg={3}>
                <Card>
                    <CardContent>
                        <CardMedia style={{ height: "120px", borderRadius: "5px" }} image={bb} />
                        <Typography variant='caption' color='textSecondary'>
                            Bert Bos
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={6} sm={6} lg={3}>
                <Card>
                    <CardContent>
                        <CardMedia style={{ height: "120px", borderRadius: "5px" }} image={be} />
                        <Typography variant='caption' color='textSecondary'>
                            Brendan Eich
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
};

export default Profile;
