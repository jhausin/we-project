import React, { useState } from "react";
import { AppBar, Tabs, Tab } from "@material-ui/core";

import Register from "./Register/Register";
import Login from "./Login/Login";
import Navigator from "./Navigator/Navigator";

import styles from "./PHP.module.scss";

const PHP: React.FC = () => {
    const [value, setValue] = useState(0);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    const handleLoginChange = () => {
        setIsLoggedIn(true);
    };
    return (
        <div className={styles.container}>
            <AppBar position='static'>
                <Tabs value={value} onChange={handleChange} scrollButtons='on' variant='scrollable'>
                    <Tab label='Register' />
                    <Tab label='Login' />
                    <Tab label='Navigator' />
                </Tabs>
            </AppBar>
            <Register value={value} index={0} />
            <Login value={value} index={1} changeLogin={handleLoginChange} />
            {isLoggedIn ? (
                <Navigator value={value} index={2} />
            ) : (
                <Login value={value} index={2} changeLogin={handleLoginChange} />
            )}
        </div>
    );
};

export default PHP;
