import React, { useState, useEffect } from "react";
import { Typography, Button, TextField, Grid } from "@material-ui/core";
import axios from "axios";
import styles from "./Navigator.module.scss";

interface Data {
    [x: string]: any;
}
const Navigator: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    const [data, setData] = useState<Data>({});
    const [edit, setEdit] = useState(false);
    const [currentTopic, setCurrentTopic] = useState("");
    const [content, setContent] = useState("");
    const [categories, setCategories] = useState<string[]>([]);
    const [currentCategory, setCurrentCategory] = useState("");
    const [references, setReferences] = useState("");
    const url: string = "http://www2.inf.h-brs.de/~jhausi2s/php/content.php";

    useEffect(() => {
        const fetch = async () => {
            fetchData();
        };
        fetch();
    }, []);

    const fetchData = async () => {
        axios.get(url).then((res) => {
            setData(res.data);
        });
    };
    const handleButtonClick = (topic: string) => {
        setCurrentTopic(topic);
        setCategories(Object.keys(data[topic]));
    };
    const changeContent = (category: string) => {
        setCurrentCategory(category);
        setContent(data[currentTopic][category]["content"]);
        setReferences(data[currentTopic][category]["references"]);
    };
    const handleEditClick = () => {
        setEdit(!edit);
        if (edit === true) {
            saveChanges();
        }
    };
    const saveChanges = () => {
        axios
            .post(
                url,
                JSON.stringify({
                    topic: currentTopic,
                    category: currentCategory,
                    content: content,
                }),
            )
            .then((res) => console.log(res));
    };
    const handleContentChange = (event: React.ChangeEvent<{ value: string }>) => {
        setContent(event.target.value as string);
    };
    return (
        <div hidden={index !== value} style={{ height: "100%" }}>
            <Grid container className={styles.container}>
                <Grid item className={styles.navbar} xs={12} lg={12}>
                    <Typography variant='h3' align='center'>
                        WWW-Navigator
                    </Typography>
                    <div className={styles.buttons}>
                        <div className={styles.topicButtons}>
                            <Button variant='contained' color='primary' onClick={() => handleButtonClick("html")}>
                                HTML
                            </Button>
                            <Button variant='contained' color='primary' onClick={() => handleButtonClick("css")}>
                                CSS
                            </Button>
                            <Button variant='contained' color='primary' onClick={() => handleButtonClick("javascript")}>
                                JavaScript
                            </Button>
                        </div>
                        <div className={styles.editButton}>
                            <Button variant='contained' color='primary' onClick={handleEditClick}>
                                {edit ? "SAVE" : "EDIT"}
                            </Button>
                        </div>
                    </div>
                </Grid>
                <Grid item className={styles.sidebar} lg={3} xs={4}>
                    <Typography variant='h6'>Kategorien</Typography>
                    {categories.map((category) => {
                        return (
                            <Button
                                key={category}
                                variant='contained'
                                color='primary'
                                onClick={() => changeContent(category)}
                            >
                                {category}
                            </Button>
                        );
                    })}
                </Grid>
                <Grid item className={styles.content} lg={6} xs={8}>
                    <div hidden={edit} className={styles.content}>
                        <Typography variant='h4' align='center' gutterBottom>
                            {currentCategory}
                        </Typography>
                        <Typography variant='body1' color='textSecondary'>
                            {content}
                        </Typography>
                    </div>
                    <div hidden={!edit}>
                        <div className={styles.editor}>
                            <Typography variant='h4' align='center' gutterBottom>
                                editor
                            </Typography>
                            <TextField
                                label={currentCategory}
                                className={styles.input}
                                variant='outlined'
                                value={content}
                                multiline
                                onChange={handleContentChange}
                            />
                        </div>
                    </div>
                </Grid>
                <Grid item className={styles.sources} lg={3} xs={12}>
                    <Typography variant='h6' gutterBottom>
                        Referenzen
                    </Typography>
                    <Typography variant='body1' color='textSecondary'>
                        {references}
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
};

export default Navigator;
