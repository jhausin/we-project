import React, { useState } from "react";
import axios from "axios";
import { Typography, TextField, Button, Snackbar, Card, CardContent } from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";

import styles from "./Register.module.scss";

const Register: React.FC<TaskPanelProps> = (props: TaskPanelProps) => {
    const { index, value } = props;
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const [registered, setRegistered] = useState(false);
    const url: string = "http://www2.inf.h-brs.de/~jhausi2s/php/registration.php";

    const handleUsernameChange = (event: React.ChangeEvent<{ value: string }>) => {
        setRegistered(false);
        setUsername(event.target.value);
    };
    const handlePasswordChange = (event: React.ChangeEvent<{ value: string }>) => {
        setRegistered(false);
        setPassword(event.target.value);
    };
    const handleClick = () => {
        registerUser();
    };
    const registerUser = async () => {
        axios
            .post(url, {
                username: username,
                password: password,
            })
            .then((res) => {
                if (res.status === 200) setRegistered(true);
            });
    };
    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setRegistered(false);
    };
    const Alert = (props: AlertProps) => {
        return <MuiAlert elevation={6} variant='filled' {...props} />;
    };

    return (
        <div hidden={index !== value}>
            <div className={styles.container}>
                <Card>
                    <CardContent>
                        <Typography variant='h4' align='center' gutterBottom>
                            Registration
                        </Typography>
                        <Typography gutterBottom>Benutzername: </Typography>
                        <TextField className={styles.button} variant='outlined' onChange={handleUsernameChange} />
                        <Typography style={{ marginTop: "5%" }} gutterBottom>
                            Password:
                        </Typography>
                        <TextField
                            className={styles.button}
                            type='password'
                            variant='outlined'
                            onChange={handlePasswordChange}
                        />
                        <Button variant='contained' color='primary' onClick={handleClick} style={{ marginTop: "5%" }}>
                            register
                        </Button>
                    </CardContent>
                </Card>
            </div>
            <Snackbar autoHideDuration={3000} open={registered} onClose={handleClose}>
                <Alert severity='success'>Registrierung erfolgreich.</Alert>
            </Snackbar>
        </div>
    );
};

export default Register;
