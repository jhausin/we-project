import React, { useState, useEffect } from "react";
import axios from "axios";
import { Typography, TextField, Button, Card, CardContent, Snackbar } from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import styles from "./Login.module.scss";
interface LoginProps extends TaskPanelProps {
    changeLogin: () => void;
}

const Login: React.FC<LoginProps> = (props: LoginProps) => {
    const { index, value, changeLogin } = props;
    const [username, setUsername] = useState<string | null>(null);
    const [password, setPassword] = useState<string | null>(null);
    const [loggedIn, setLoggedIn] = useState(false);
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState(false);
    const url: string = "http://www2.inf.h-brs.de/~jhausi2s/php/login.php";

    const handleUsernameChange = (event: React.ChangeEvent<{ value: string }>) => {
        setUsername(event.target.value);
    };
    const handlePasswordChange = (event: React.ChangeEvent<{ value: string }>) => {
        setPassword(event.target.value);
    };
    const handleClick = () => {
        login();
    };
    const login = async () => {
        axios
            .post(url, {
                username: username,
                password: password,
            })
            .then((res) => {
                console.log(res);
                if (res.data.login) {
                    setLoggedIn(true);
                    setSuccess(true);
                    changeLogin();
                } else {
                    setError(true);
                }
            });
    };
    const Alert = (props: AlertProps) => {
        return <MuiAlert elevation={6} variant='filled' {...props} />;
    };

    useEffect(() => {
        setUsername(null);
        setPassword(null);
    }, []);
    return (
        <div hidden={index !== value}>
            <div className={styles.container}>
                <div hidden={loggedIn}>
                    <Card>
                        <CardContent>
                            <Typography variant='h4' align='center' gutterBottom>
                                Login
                            </Typography>
                            <Typography gutterBottom>Benutzername: </Typography>
                            <TextField className={styles.button} variant='outlined' onChange={handleUsernameChange} />
                            <Typography style={{ marginTop: "5%" }} gutterBottom>
                                Password:
                            </Typography>
                            <TextField
                                className={styles.button}
                                type='password'
                                variant='outlined'
                                onChange={handlePasswordChange}
                            />
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleClick}
                                style={{ margin: "5% 0" }}
                            >
                                login
                            </Button>
                            <Typography>Noch kein Account vorhanden? Einfach registrieren.</Typography>
                        </CardContent>
                    </Card>
                </div>
                <div hidden={!loggedIn}>
                    <Typography variant='h5' gutterBottom>
                        Welcome back {username}
                    </Typography>
                    <Button variant='contained' color='secondary' onClick={() => setLoggedIn(false)}>
                        Logout
                    </Button>
                </div>
            </div>
            <Snackbar autoHideDuration={3000} open={error} onClose={() => setError(false)}>
                <Alert severity='error'>Benutzername oder Passwort falsch.</Alert>
            </Snackbar>
            <Snackbar autoHideDuration={2000} open={success} onClose={() => setSuccess(false)}>
                <Alert severity='success'>Login erfolgreich</Alert>
            </Snackbar>
        </div>
    );
};

export default Login;
