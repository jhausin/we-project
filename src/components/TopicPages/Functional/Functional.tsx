import React, { useState } from "react";
import { Typography, Button } from "@material-ui/core";
import { stopwords } from "./variables";
import styles from "./Functional.module.scss";

const Functional: React.FC = () => {
    const url = "https://kaul.inf.h-brs.de/we/assets/html/plagiatsresolution.html";
    const [hidden, setHidden] = useState(true);
    const [topThree, setTopThree] = useState([
        ["", 0],
        ["", 0],
        ["", 0],
    ]);
    const handleClick = (): void => {
        const sourcecode: string = fetchSourceCode();
        setTopThree(findTopThree(sourcecode));
        setHidden(false);
    };
    const fetchSourceCode = (): string => {
        const httpRequest: XMLHttpRequest = new XMLHttpRequest();
        httpRequest.open("GET", url, false);
        httpRequest.send();
        return httpRequest.responseText;
    };
    const findTopThree = (sourcecode: string): [string, number][] => {
        const filteredSourceCode: string = sourcecode
            .replaceAll(/<([^>]*)>/gi, "") //replace all html tags
            .replaceAll(/([^a-zA-Zäöüß])/g, " ") //replace all non words
            .replaceAll(/\s\s+/g, " ") //replace all multiple whitespaces with a single whitespace
            .trim()
            .toLowerCase(); //only lower case words => filtering stopwords later
        const filteredString: string[] = filteredSourceCode.split(" ").filter((word) => !stopwords.includes(word));
        return Array.from(
            filteredString
                .reduce((map: Map<string, number>, word: string) => {
                    map.get(word) === undefined ? map.set(word, 1) : map.set(word, map.get(word)! + 1);
                    return map;
                }, new Map())
                .entries(),
        )
            .sort((a, b) => b[1] - a[1])
            .slice(0, 3);
    };
    return (
        <div className={styles.container}>
            <Typography variant='h4' align='center' gutterBottom>
                Textanalyse mit filter-map-reduce
            </Typography>

            <div className={styles.input}>
                <Button color='primary' variant='contained' style={{ maxWidth: "200px" }} onClick={handleClick}>
                    Quellcode filtern
                </Button>
            </div>
            <Typography variant='body2' align='center' color='textSecondary' gutterBottom>
                URL: https://kaul.inf.h-brs.de/we/assets/html/plagiatsresolution.html
            </Typography>
            <div hidden={hidden}>
                <div className={styles.list}>
                    <Typography variant='h5' align='center'>
                        Die drei häufigsten Begriffe im Text:
                    </Typography>
                    <ol>
                        <li>
                            <Typography>
                                {topThree[0][0]} - {topThree[0][1]}x
                            </Typography>
                        </li>
                        <li>
                            <Typography>
                                {topThree[1][0]} - {topThree[1][1]}x
                            </Typography>
                        </li>
                        <li>
                            <Typography>
                                {topThree[2][0]} - {topThree[2][1]}x
                            </Typography>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    );
};

export default Functional;
