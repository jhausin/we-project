import React from "react";

import styles from "./Home.module.scss";
const Home: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={styles.quote}>
                <h1>
                    “Web design is not just about creating pretty layouts.
                    <br /> It’s about understanding the marketing challenge behind your business.”
                </h1>
                <p> – Mohamed Saad</p>
            </div>
        </div>
    );
};

export default Home;
