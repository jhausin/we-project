import React from "react";
import styles from "./About.module.scss";
const About: React.FC = () => {
    return (
        <div className={styles.content}>
            <div className={styles.about}>
                <h1>Über dieses Projekt.</h1>
                <p>
                    Mein Name ist Jannik Hausin und ich studiere Informatik an der Hochschule Bonn-Rhein-Sieg(H-BRS).
                    <br />
                    Diese Seite dient als Hausarbeit für das Modul "Web Engineering" (Prof. Dr. Manfred Kaul) im
                    Wintersemester 2020/2021.
                    <br />
                    Weitere Informationen finden Sie auf der Gitlab-Seite des Projekts.
                </p>
            </div>
        </div>
    );
};

export default About;
