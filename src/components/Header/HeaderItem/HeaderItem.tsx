import React from "react";
import styles from "./HeaderItem.module.scss";
import { Link } from "react-router-dom";
interface HeaderItemProps {
    name: string;
    link: string;
}
const HeaderItem: React.FC<HeaderItemProps> = (props: HeaderItemProps) => {
    const { name, link } = props;
    return (
        <Link className={styles.item} to={link}>
            {name}
        </Link>
    );
};

export default HeaderItem;
