import React from "react";
import { Link } from "react-router-dom";

import styles from "./Header.module.scss";
import HeaderItem from "./HeaderItem/HeaderItem";
import logo from "../../assets/logo_black.png";
import gitlab from "../../assets/gitlab-icon-rgb.svg";

const Header: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={styles.image}>
                <Link to='/home'>
                    <img src={logo} alt='logo' />
                </Link>
            </div>
            <div className={styles.items}>
                <HeaderItem name='About' link='/about' />
                <HeaderItem name='Exercises' link='/projects' />
                <a href='https://gitlab.com/jhausin/we-project'>
                    <img src={gitlab} alt='gitlab' />
                </a>
            </div>
        </div>
    );
};

export default Header;
