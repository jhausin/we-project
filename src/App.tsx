import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { PageTransition } from "@steveeeie/react-page-transition";

import styles from "./App.module.scss";
import {
    Home,
    About,
    Documents,
    Header,
    Projects,
    Introduction,
    CSS1,
    CSS2,
    JavaScript,
    Functional,
    EcmaScript,
    Async,
    DOM,
    Vue,
    PHP,
} from "./components";

const App = () => {
    return (
        <Router basename='/~jhausi2s/#/'>
            <div className={styles.container}>
                <Header />
                <div className={styles.content}>
                    <Route
                        render={({ location }) => {
                            return (
                                <PageTransition preset='moveToLeftFromRight' transitionKey={location.pathname}>
                                    <Switch location={location}>
                                        <Route exact path='/' render={() => <Redirect to='/home' />} />
                                        <Route exact path='/home' component={Home} />
                                        <Route exact path='/about' component={About} />
                                        <Route exact path='/projects' component={Projects} />
                                        <Route exact path='/documentation' component={Documents} />
                                        <Route exact path='/projects/exercise1' component={Introduction} />
                                        <Route exact path='/projects/exercise2' component={CSS1} />
                                        <Route exact path='/projects/exercise3' component={CSS2} />
                                        <Route exact path='/projects/exercise4' component={JavaScript} />
                                        <Route exact path='/projects/exercise5' component={EcmaScript} />
                                        <Route exact path='/projects/exercise6' component={Functional} />
                                        <Route exact path='/projects/exercise7' component={DOM} />
                                        <Route exact path='/projects/exercise8' component={Async} />
                                        <Route exact path='/projects/exercise9' component={Vue} />
                                        <Route exact path='/projects/exercise10' component={PHP} />
                                    </Switch>
                                </PageTransition>
                            );
                        }}
                    />
                </div>
            </div>
        </Router>
    );
};

export default App;
